export const restoData = [
    {
      id: '1',
      title: 'Pizza Palace',
      description: 'Serving delicious pizzas with a variety of toppings. Located on Main Street.',
      menus: [
        { id: '1', name: 'Margherita Pizza', description: 'Classic pizza topped with tomato sauce, mozzarella cheese, and fresh basil.', price: 'Rp 50,000', imageUrl: 'https://images.unsplash.com/photo-1589187151053-5ec8818e661b?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OXx8bWFyZ2VyaXRhJTIwcGl6emF8ZW58MHx8MHx8fDA%3D' },
        { id: '2', name: 'Pepperoni Pizza', description: 'Pizza topped with spicy pepperoni slices and melted cheese.', price: 'Rp 55,000', imageUrl: 'https://images.unsplash.com/photo-1534308983496-4fabb1a015ee?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8bWFyZ2VyaXRhJTIwcGl6emF8ZW58MHx8MHx8fDA%3D' },
        { id: '3', name: 'Vegetarian Pizza', description: 'Pizza loaded with assorted vegetables and cheese, perfect for vegetarians.', price: 'Rp 55,000', imageUrl: 'https://images.unsplash.com/photo-1528830984461-4d5c3cc1abf0?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTJ8fG1hcmdlcml0YSUyMHBpenphfGVufDB8fDB8fHww' },
        { id: '4', name: 'Hawaiian Pizza', description: 'Pizza topped with ham, pineapple, and cheese for a tropical twist.', price: 'Rp 60,000', imageUrl: 'https://images.unsplash.com/photo-1565299624946-b28f40a0ae38?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Nnx8bWFyZ2VyaXRhJTIwcGl6emF8ZW58MHx8MHx8fDA%3D' },
        { id: '5', name: 'Meat Lovers Pizza', description: 'Pizza loaded with a variety of meats such as sausage, pepperoni, and bacon.', price: 'Rp 65,000', imageUrl: 'https://images.unsplash.com/photo-1600628421055-4d30de868b8f?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTh8fG1hcmdlcml0YSUyMHBpenphfGVufDB8fDB8fHww' },
      ],
    },
    {
      id: '2',
      title: 'Sushi Sake',
      description: 'Authentic Japanese sushi restaurant offering a wide selection of sushi rolls and sashimi.',
      menus: [
        { id: '1', name: 'California Roll', description: 'Traditional sushi roll filled with crab, avocado, and cucumber.', price: 'Rp 45,000', imageUrl: 'https://images.unsplash.com/photo-1579584425555-c3ce17fd4351?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8N3x8Y2FsbGlmb3JuaWElMjByb2xsJTIwc3VzaGl8ZW58MHx8MHx8fDA%3D' },
        { id: '2', name: 'Spicy Tuna Roll', description: 'Sushi roll filled with spicy tuna and topped with spicy mayo sauce.', price: 'Rp 50,000', imageUrl: 'https://images.unsplash.com/photo-1607247098729-accb1f156620?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OXx8Y2FsbGlmb3JuaWElMjByb2xsJTIwc3VzaGl8ZW58MHx8MHx8fDA%3D' },
        { id: '3', name: 'Salmon Nigiri', description: 'Classic Japanese sushi made with fresh salmon atop a small ball of rice.', price: 'Rp 60,000', imageUrl: 'https://images.unsplash.com/photo-1562158074-d49fbeffcc91?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTF8fGNhbGxpZm9ybmlhJTIwcm9sbCUyMHN1c2hpfGVufDB8fDB8fHww' },
        { id: '4', name: 'Tempura Shrimp Roll', description: 'Sushi roll filled with crispy tempura shrimp and avocado.', price: 'Rp 55,000', imageUrl: 'https://images.unsplash.com/photo-1607247098912-8b2b44fde4d6?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8M3x8Y2FsbGlmb3JuaWElMjByb2xsJTIwc3VzaGl8ZW58MHx8MHx8fDA%3D' },
        { id: '5', name: 'Dragon Roll', description: 'Special sushi roll featuring eel, avocado, and cucumber topped with sliced avocado and eel sauce.', price: 'Rp 65,000', imageUrl: 'https://images.unsplash.com/photo-1617196035154-1e7e6e28b0db?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjB8fGNhbGxpZm9ybmlhJTIwcm9sbCUyMHN1c2hpfGVufDB8fDB8fHww' },
      ],
    },
    {
      id: '3',
      title: 'Burger Barn',
      description: 'Casual dining restaurant famous for its juicy burgers and crispy fries.',
      menus: [
        { id: '1', name: 'Classic Cheeseburger', description: 'Juicy beef patty topped with melted cheese, lettuce, tomato, and pickles.', price: 'Rp 40,000', imageUrl: 'https://images.unsplash.com/photo-1565299507177-b0ac66763828?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8OHx8YnVyZ2VyfGVufDB8fDB8fHww' },
        { id: '2', name: 'Bacon Avocado Burger', description: 'Burger with crispy bacon, creamy avocado, lettuce, and tomato.', price: 'Rp 45,000', imageUrl: 'https://images.unsplash.com/photo-1596649299486-4cdea56fd59d?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTZ8fGJ1cmdlcnxlbnwwfHwwfHx8MA%3D%3D' },
        { id: '3', name: 'Mushroom Swiss Burger', description: 'Burger topped with sautéed mushrooms, melted Swiss cheese, and tangy sauce.', price: 'Rp 45,000', imageUrl: 'https://plus.unsplash.com/premium_photo-1672363353886-a106864f5cb9?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTN8fGJ1cmdlcnxlbnwwfHwwfHx8MA%3D%3D' },
        { id: '4', name: 'BBQ Chicken Burger', description: 'Grilled chicken breast smothered in barbecue sauce, topped with lettuce and onion rings.', price: 'Rp 45,000', imageUrl: 'https://images.unsplash.com/photo-1551782450-a2132b4ba21d?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTF8fGJ1cmdlcnxlbnwwfHwwfHx8MA%3D%3D' },
        { id: '5', name: 'Veggie Burger', description: 'Vegetarian burger patty made with black beans, corn, and spices, served with lettuce and tomato.', price: 'Rp 40,000', imageUrl: 'https://images.unsplash.com/photo-1603064752734-4c48eff53d05?w=400&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Nnx8YnVyZ2VyfGVufDB8fDB8fHww' },
      ],
    },
  ];
  