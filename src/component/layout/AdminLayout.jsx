import React from 'react'
import SideBar from '../SideBar'
import Navbar from '../Navbar'

export default function AdminLayout() {
  return (
    <div>
       <Navbar/>
       <SideBar/>
    </div>
  )
}
