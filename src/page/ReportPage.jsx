import React from 'react'
import { Typography } from 'antd'
import Accordion from '../component/Accordion'

export default function ReportPage() {
  return (
    <div>
      <Typography.Title>Report Page</Typography.Title>
      <Accordion/>
    </div>
  )
}
