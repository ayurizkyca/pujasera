import React from 'react'
import SideBar from "../component/SideBar"
import Navbar from "../component/Navbar"

export default function HomePage() {
  return (
    <div>
      <Navbar/>
      <SideBar/>
    </div>
  )
}
