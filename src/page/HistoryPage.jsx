import React from 'react'
import { Typography } from 'antd'
import TableCustom from '../component/Table'

export default function HistoryPage() {
  return (
    <div>
        <Typography.Title>History Page</Typography.Title>
        <TableCustom/>
    </div>
  )
}
