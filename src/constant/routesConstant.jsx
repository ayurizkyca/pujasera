export const ROUTES = {
    LOGIN : '/login',
    HOME : '/home',
    HISTORY : '/home/history',
    PORTAL_RESTO : '/home/portal-resto',
    REPORT : '/home/report',
    DETAIL_RESTO : '/home/detail-resto'
} 


// export const ROUTES = {
//     LOGIN: '/login',
//     HOME: '/',
//     PORTAL_RESTO: '/portal-resto',
//     HISTORY: '/history',
//     REPORT: '/report',
//     DETAIL_PUB: '/detail-pub/:id',
//     DETAIL_RESTO: '/detail-resto/:id',
//     NOT_FOUND: '*',
//   };